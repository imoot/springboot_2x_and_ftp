package com.plugin.ftp.support;

/**
 * @author imoot@gamil.com
 * @date 2019/4/3 0003 8:57
 */
public interface FtpConstants {

    //ftp文件路径编码格式
    String DEFAULT_FTP_PATH_ENCODING="ISO-8859-1";

}

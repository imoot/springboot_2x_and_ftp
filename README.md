# SpringBoot2.x加FTP

#### 介绍
SpringBoot2.x版本实现了ftp连接池上传

#### 安装教程

1. 安装maven
2. 安装idea（可跳过）
3. 安装git

#### 使用说明

1. 把项目克隆到本地；
2. 导入idea中，使用maven打为jar
3. 或者进入项目根目录，使用mvn clean package打为jar